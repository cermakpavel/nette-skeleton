ISimplySell.com - Nette Skeleton
=================

This is a simple, skeleton application using the [Nette](https://nette.org). This is meant to
be used as a starting point for our new projects within ISimplySell.com company.

[Nette](https://nette.org) is a popular tool for PHP web development.
It is designed to be the most usable and friendliest as possible. It focuses
on security and performance and is definitely one of the safest PHP frameworks.

If you like Nette, **[please make a donation now](https://nette.org/donate)**. Thank you!

## Changes against official Nette Skeleton

- Folders *temp* and *log* are stored within *var* subdirectory
- config is moved to PROJECT_DIR
- configuration for extensions and services are moved to separate files and included within common.neon
- templates subdirectory is moved into app directory
- basic code style is added (and launched via terminal command `bin/code-style`)

